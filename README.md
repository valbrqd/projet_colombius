==================================================================================================================================================================
==================================================================================================================================================================

                                                    DOSSIER TECHNIQUE

API Web :
- le fichier index `index.html` est la base de notre API WEB. Il a été mis en forme à l'aide du fichier `style.css` et du fichier `script.js` qui contient les fonctions javascripts permettant de faire fonctionner l'API. De plus, le fichier server.js permet de lancer le serveur web sur le port 8080 et de lancer les scripts python nécessaires au fonctionnement de l'API. Le fichier `get_IP.sh` permet de récupérer l'adresse IP du Raspberry Pi sur lequel est lancé le serveur web pour pouvoir ensuite l'envoyer en tant que paramètre au fichier `server.js`. 

* Installation des packages nécessaires:
```	bash
sudo apt install nodejs
sudo apt install npm
```

POSITION :

- Le programme reconnaissance_formes_camera.py permet de détecter des formes pré-définies. Dans notre cas, nous cherchons à trouver une étoile ou un pentagone (peut etre reconnu comme un hexagone), ou un triangle ou un carré (peut etre reconnu comme un rectangle ). 

Nous utilisons, majoritairement la bibliothèque openCV avec imutils pour le traitement d'image ainsi que numpy, et time pour des calculs et os pour la gestion des images enregistrees. *

Ce programme est composé d'une classe "ShapeDetector" dans laquelle il y a une fonction "detect" pour calculer les sommets,  et nommer les formes, une fonction "text_display" pour afficher les textes dans la bonne couleur sur l'image détectée lors des tests sur PC.

Pour ces derniers, il suffit de décommenter la ligne #233. Cette ligne permet de sauvegarder l'image avec les contours des formes et leur nom selon leur niveau de priorité (principale ou autre). 
Pour les tests sur RaspBerry, il suffit d'observer ce que renvoie les print lignes #85 et #87.

reconnaissance_formes_camera.py associe un numero de mur à une forme (triangle-0, pentagone-1, carre-2, etoile-3). Le numero de la forme principale est envoyé à Positionnement.py pour lui permettre d'orienter les murs correctement et de trouver la position exacte du robot.
Cet appel à la fonction positionnement() permet d'obtenir les coordonnées x et y du robot et de les renvoyer à l'API.

* Installation des bibliothèques utiles
opencv : pip install opencv-python
imutils,numpy : pip install [nom_bibliotheque]
time : pip install python-time
os : pip install os-sys

- Le programme de Positionnement permet, à partir de l'information du mur se situant derrière le robot issu du programme reconnaissance_formes_camera.py, 
de calculer la position du robot dans la pièce, grâce à une segmentation de la pièce en murs. Ce programme utilise la librairie rpLidar pour la 
lecture d'informations, ainsi que la librairie matplotlib pour la visualisation des nuages de points. Pour que le positionnement soit fonctionnel, 
il reste simplement à appeler la fonction reconnaissance_formes_camera.py, c'est à dire associer l'appel réalisé dans script.js à une touche du 
clavier par exemple, en sachant que les outputs de reconnaissance_formes_camera.py sont les coordonnees transformees du robot pour qu'elles 
correspondent à la mini-map. Sinon, les deux dernieres lignes du code Positionnement.py peuvent etre decommentees pour observer le traitement 
effectuer un nuages representatif d'un cas reel prenregistre lors des phases de tests.

* Installation des bibliothèques utiles
rpLidar : os : pip install rpLidar

MOUVEMENT AUTOMATIQUE :

- Depuis l'API Web, un clic sur la mini-map déclenche le calcul de la trajectoire, c'est-à-dire, le calcul des rotations et des 
distances à effectuer/parcourir par le robot. Les informations de déplacement seront transmises sur la liaison UART /dev/ttyUSB0. 
Des tests sur la reception de ces donnees restent a effectuer depuis le programme en C. 

- //COMPLETER info code en C// 

MOUVEMENT MANUEL :
- Depuis l'API Web, les touches z,q,s,d, permettent de déplacer le robot, et les touches i,j,k,l, permettent de choisir l'orientation de la caméra. Le fichier `script.js` permet de faire une requête POST au fichier `server.js` qui lui s'occupera de run les fichiers python afin que le raspberry envoie les informations à la carte STM32F3 via la liaison UART /dev/ttyUSB0.

- Une fois les informations reçues par la carte STM32F3, 
//COMPLETER//

FLUX VIDEO : 
- Directement sur le Raspberry, il faut effectuer des commandes dans le but de lancer le logicel motion et donc émettre grâce à une certaine IP (celle de la personne qui effectue le partage de connexion).

Pour lancer l'écoute : sudo ./get_IP.sh | xargs node server.js & motion (Pas forcément nécessaire si on vient de mettre le raspberry sous tension, cette commande s'effectuera automatiquement)

La première commande permet de récupérer l'IP de la personne effectuant le partage de connexion, et la deuxième commande lance l'écoute de la vidéo. Pour voir la vidéo, il faut ensuite aller sur le server web.

* Installation du package motion sur le Raspberry : 
sudo install motion

* Confiduration du fichier motion.conf :
sudo nano etc/motion/motion.conf
==================================================================================================================================================================
==================================================================================================================================================================