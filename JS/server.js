const express = require('express');
const app = express();
const port = 3000;
const cors = require('cors');
const { spawn } = require('child_process');
const ip = process.argv[2];


app.get('/', function(req, res) {
  res.sendFile('/home/admin/projet_colombius/index.html');
  
});

app.use(cors({
    origin: 'http://${ip}:5500'
}));
app.use(express.json());

app.post('/key', (req, res) => {
    console.log(req.body);
    res.send('Data received');
});

app.listen(port, () => {
    console.log(`Ecoute sur http://${ip}:${port}`)
});


  app.post('/run-python-move', (req, res) => {
    let args = ['z', 'q', 's', 'd', 'Z', 'S','i','k','j','l'];
    console.log (args.includes(req.body.key));
    if(args.includes(req.body.key)){
      const pythonProcess = spawn('python', ['/home/admin/projet_colombius/PYTHON/move.py', req.body.key]);
      
      // Gérez les sorties du processus Python
      pythonProcess.stdout.on('data', (data) => {
        console.log(`${data}`);
      });
      
      pythonProcess.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
      });
      
      // Gérez la fin du processus Python
      pythonProcess.on('close', (code) => {
        res.send(`Code python bien exécuté`);
      });
    }
    else{
      res.send(`Code python mal exécuté`);  
    }
    });




app.get('/run-python-auto', (req, res) => {
  const pythonProcess = spawn('python', ['/home/admin/projet_colombius/PYTHON/reconnaissance_formes_camera.py']);
  // Gérez les sorties du processus Python pour le code de reconnaisance de formes
  let reponse = '';
  pythonProcess.stdout.on('data', (data) => {
    const output = `${data}`;
    reponse += output;
    console.log(reponse);
    res.send(reponse);
  });
  
  pythonProcess.stderr.on('data', (data) => {
    console.error(`stderr: ${data}`);
  });
  
  pythonProcess.on('close', (code) => {
    res.send(reponse);
  });
});

app.post('/run-python-auto', (req, res) => {
  const autoSTM = spawn('python', ['/home/admin/projet_colombius/PYTHON/reconnaissance_formes_camera.py',req.body.key]);
  
  autoSTM.stdout.on('data', (data) => {
    console.log(`${data}`);
  });
  
  autoSTM.stderr.on('data', (data) => {
    console.error(`stderr: ${data}`);
  });
  
  // Gérez la fin du processus Python
  autoSTM.on('close', (code) => {
    res.send(`Code python bien exécuté`);
  });
});

app.post('/run-python-lamp', (req,res) => {
  const LED = spawn('python', ['/home/admin/projet_colombius/PYTHON/led.py',req.body.lamp]);
  
  LED.stdout.on('data', (data) => {
    console.log(`${data}`);
  });

  LED.stderr.on('data', (data) => {
    console.error(`stderr: ${data}`);
  });
  
  // Gérez la fin du processus Python
  LED.on('close', (code) => {
    res.send(`Code python bien exécuté`);
  });
});

app.post('/run-python-deplacement', (req,res) => {
  const pythonProcess = spawn('python', ['/home/admin/projet_colombius/PYTHON/Map/map.py',req.body.x,req.body.y]);

  pythonProcess.stdout.on('data', (data) => {
    console.log(`${data}`);
  }
  );

  pythonProcess.stderr.on('data', (data) => {
    console.error(`stderr: ${data}`);
  }
  );

  // Gérez la fin du processus Pytho
  pythonProcess.on('close', (code) => {
    res.send(`Code python bien exécuté`);
  }
  );
});
 

