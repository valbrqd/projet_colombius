const ip = process.argv[2];


document.addEventListener("keydown", function(event) {
  if (event.key == 'z' || event.key == 'q' || event.key == 's' || event.key == 'd' ) {
    sendKey(event.key);
  }
});

const interrupt = document.getElementById("self");

interrupt.addEventListener("change",function(){
if(this.checked){
  sendKey("on");
}
else{
  sendKey("off");
}
});




function sendKey(key) {

  const data = {
    key: key
  };

  fetch('http://localhost:3000/run-python', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })

  .then(response => response.text())
  .then(data => {
    console.log('Success:', data);
  })
  .catch((error) => {
    console.error('Error:', error); 
  });
}

  function switchDiv() {
    var control = document.getElementById('controls');
    var vid = document.getElementById('video');
  
    if (control.style.display !== "none") {
      control.style.display = "none";
      vid.style.display = "block";
    } else {
      control.style.display = "block";
    }
  }

 
  