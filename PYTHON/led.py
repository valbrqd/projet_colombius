from gpiozero import LED
from time import sleep
import sys
import RPi.GPIO as GPIO

# led_state = sys.argv[1]
# led = LED(2)  
# print("LED")
# print(led.is_lit)
# print(sys.argv[1])
# print (led_state)
# if led_state == 'true':
#     led_state = False
#     led.on()
#     print("LED is on")
# else:
#     led_state = True
#     led.off()
#     print("LED is off")

led_pin = 2

GPIO.setmode(GPIO.BCM)
GPIO.setup(led_pin, GPIO.OUT, initial=GPIO.LOW)
print("LED state:", GPIO.input(led_pin))

if GPIO.input(led_pin) == GPIO.LOW: 
    GPIO.output(led_pin, GPIO.HIGH) 
else:
    GPIO.output(led_pin, GPIO.LOW)
    
    

