#from tkinter import *
import time
import numpy as np
import math
from PIL import ImageGrab

"""
Classe permettant le calcul de la trajectoire du robot
"""
class Robot():
    def __init__(self, x, y, vitesse, long_map, largeur_map):
        # Initialise les attributs du robot avec les valeurs passées en paramètre
        self.x = x
        self.y = y
        self.vitesse = vitesse
        self.x_dest = []
        self.y_dest = []
        self.angle = 0
        self.dimension = (25, 25)
        self.long_map = long_map
        self.largeur_map = largeur_map

    def set_position_init_robot(self):
        # Affiche l'angle initial du robot
        self.position()
        print("L'angle initial est de ", self.angle, "°")

    def get_pos_robot(self):
        # Retourne les coordonnées actuelles du robot
        return self.x, self.y

    def position(self):
        # Lit les informations de position à partir d'un fichier
        fichier = open('infos_positions.txt', 'r')
        lines = fichier.read().split('\n')
        for l in range(len(lines)):
            lines[l] = lines[l].split(' ')

        # Met à jour l'angle en fonction des informations lues
        self.angle = float(lines[1][0]) * 90 + float(lines[2][0]) - 180

        if self.angle < 0:
            self.angle = 360 + self.angle

        # Met à jour les coordonnées du robot en fonction des informations lues
        if lines[0][0] == '-1' and lines[1][0] == '0':
            self.x = float(lines[0][1]) / 10
            self.y = float(lines[1][1]) / 10

        if lines[0][0] == '0' and lines[1][0] == '1':
            self.x = self.largeur_map - float(lines[0][1]) / 10
            self.y = float(lines[1][1]) / 10

        if lines[0][0] == '1' and lines[1][0] == '2':
            self.x = self.largeur_map - float(lines[0][1]) / 10
            self.y = self.long_map - float(lines[1][1]) / 10

        if lines[0][0] == '2' and lines[1][0] == '3':
            self.x = float(lines[0][1]) / 10
            self.y = self.long_map - float(lines[1][1]) / 10

        fichier.close()

    def start(self):
        # Démarre le mouvement du robot en définissant la destination et en envoyant les informations de mouvement
        self.set_way()
        rotation, dist = self.envoi_infos_mouvement()

        return rotation, dist

    def chemin_robot(self):
        # Méthode qui gère le chemin du robot jusqu'à sa destination
        if self.x != -20 and self.y != -20 and len(self.x_dest) != 0 and len(self.y_dest) != 0:
            self.mouvement_robot()# Déplace le robot
            self.set_pos_robot() # Met à jour la position graphique du robot sur le canvas
            if (self.x != self.x_dest[-1]) or (self.y != self.y_dest[-1]):
                self.canvas.after(10, self.chemin_robot)# Appel récursif pour continuer le chemin
            else:
                del self.x_dest[-1]# Supprime le dernier point de destination atteint
                del self.y_dest[-1]
                self.chemin_robot
    
    def mouvement_robot(self):
        # Méthode qui gère le mouvement du robot vers la prochaine destination
        if len(self.y_dest) != 0:
            if self.x != self.x_dest[-1]:
                if self.x < self.x_dest[-1]:
                    self.x = self.x + self.vitesse  # Déplacement horizontal vers la droite
                if self.x > self.x_dest[-1]:
                    self.x = self.x - self.vitesse  # Déplacement horizontal vers la gauche

        if self.y != self.y_dest[-1]:
            if self.y < self.y_dest[-1]:
                self.y += self.vitesse  # Déplacement vertical vers le bas
            if self.y > self.y_dest[-1]:
                self.y -= self.vitesse  # Déplacement vertical vers le haut


    def set_destination(self, x, y):
        self.x_dest.append(x)
        self.y_dest.append(y)

    def set_way(self):

        # Méthode pour définir le chemin du robot en utilisant des points intermédiaires
        p = (self.y_dest[-1] - self.y) / (self.x_dest[0] - self.x)  # Pente du segment entre la position actuelle et la première destination
        c = self.y - p * self.x  # Coefficient de l'ordonnée à l'origine de la droite

        n_traj = 200  # Nombre de points pour décrire le chemin
        n_seg = int(n_traj / 2)  # Nombre de segments pour diviser le chemin

        lst_x = list(np.linspace(min(self.x, self.x_dest[0]), max(self.x, self.x_dest[0]), n_traj))  # Coordonnées x des points intermédiaires
        lst_y = []
        for x in lst_x:
            lst_y.append(p * x + c)  # Coordonnées y des points intermédiaires calculées à partir de l'équation de la droite

        checkpoint = [(int)(lst_x[n_seg - 1]), (int)(lst_y[n_seg - 1])]  # Point de contrôle pour éviter les obstacles
        n = [-p * (600 - checkpoint[0]), 600 - checkpoint[1]]  # Vecteur normalisé pour déplacer le point de contrôle
        norm_n = math.sqrt(n[0] ** 2 + n[1] ** 2)  # Norme du vecteur n
        n = [n[0] / norm_n, n[1] / norm_n]  # Vecteur normalisé

        lst_x1 = list(
            np.linspace(min(min(self.x, self.x_dest[0]), checkpoint[0]), max(min(self.x, self.x_dest[0]), checkpoint[0]),
                        n_seg))  # Coordonnées x des points intermédiaires pour le premier segment
        lst_y1 = lst_y[:n_seg - 1]  # Coordonnées y des points intermédiaires pour le premier segment
        lst_x2 = list(
            np.linspace(min(max(self.x, self.x_dest[0]), checkpoint[0]), max(max(self.x, self.x_dest[0]), checkpoint[0]),
                        n_seg))  # Coordonnées x des points intermédiaires pour le deuxième segment
        lst_y2 = lst_y[n_seg:]  # Coordonnées y des points intermédiaires pour le deuxième segment

        pente1 = (checkpoint[1] - lst_y1[0]) / (checkpoint[0] - lst_x1[0])  # Pente du premier segment
        pente2 = pente1  # Pente du deuxième segment

        c1 = lst_y1[0] - pente1 * lst_x1[0]  # Coefficient de l'ordonnée à l'origine pour le premier segment
        c2 = lst_y2[0] - pente2 * lst_x2[0]  # Coefficient de l'ordonnée à l'origine pour le deuxième segment

        lst_y1 = []
        lst_y2 = []
        for i in range(n_seg):
            lst_y1.append(pente1 * lst_x1[i] + c1)  # Recalcule les coordonnées y pour le premier segment
            lst_y2.append(pente2 * lst_x2[i] + c2)  # Recalcule les coordonnées y pour le deuxième segment

        cleared_way1 = False
        cleared_way2 = False

        while cleared_way1 == False or cleared_way2 == False:
            cleared_way1 = True
            cleared_way2 = True

            for p1 in range(len(lst_x1)):
                if cleared_way1 == True:
                    if (int)(lst_x1[p1]) >= (self.largeur_map / 2) - 30 and (int)(lst_x1[p1]) <= (self.largeur_map / 2) + 30 and (int)(lst_y1[p1]) >= (self.long_map / 2) - 30 and (int)(lst_y1[p1]) <= (self.long_map / 2) + 30:
                        
                        cleared_way1 = False
                        checkpoint[0] += 10 * n[0]  # Déplace le point de contrôle
                        checkpoint[1] += 10 * n[1]
                        lst_x1 = list(np.linspace(lst_x1[0], checkpoint[0], n_seg))  # Recalcule les coordonnées x pour le premier segment
                        pente1 = (checkpoint[1] - lst_y1[0]) / (checkpoint[0] - lst_x1[0])  # Recalcule la pente pour le premier segment
                        c1 = lst_y1[0] - pente1 * lst_x1[0]  # Recalcule le coefficient de l'ordonnée à l'origine pour le premier segment
                        lst_x2 = list(np.linspace(checkpoint[0], lst_x2[-1], n_seg))  # Recalcule les coordonnées x pour le deuxième segment
                        pente2 = (lst_y2[-1] - checkpoint[1]) / (lst_x2[-1] - checkpoint[0])  # Recalcule la pente pour le deuxième segment
                        c2 = checkpoint[1] - pente2 * checkpoint[0]  # Recalcule le coefficient de l'ordonnée à l'origine pour le deuxième segment
                        lst_y1.clear()  # Efface les coordonnées y pour le premier segment
                        lst_y2.clear()  # Efface les coordonnées y pour le deuxième segment

                        for i in range(n_seg):
                            lst_y1.append(pente1 * lst_x1[i] + c1)  # Recalcule les coordonnées y pour le premier segment
                            lst_y2.append(pente2 * lst_x2[i] + c2)  # Recalcule les coordonnées y pour le deuxième segment

            for p2 in range(len(lst_x2)):
                if cleared_way2 == True:
                    if (int)(lst_x2[p2]) >= (self.largeur_map / 2) - 30 and (int)(lst_x2[p2]) <= (self.largeur_map / 2) + 30 and (int)(lst_y2[p2]) >= (self.long_map / 2) - 30 and (int)(lst_y2[p2]) <= (self.long_map / 2) + 30:
                        
                        cleared_way2 = False
                        checkpoint[0] += 10 * n[0]  # Déplace le point de contrôle
                        checkpoint[1] += 10 * n[1]
                        lst_x1 = list(np.linspace(lst_x1[0], checkpoint[0], n_seg))  # Recalcule les coordonnées x pour le premier segment
                        pente1 = (checkpoint[1] - lst_y1[0]) / (checkpoint[0] - lst_x1[0])  # Recalcule la pente pour le premier segment
                        c1 = lst_y1[0] - pente1 * lst_x1[0]  # Recalcule le coefficient de l'ordonnée à l'origine pour le premier segment
                        lst_x2 = list(np.linspace(checkpoint[0], lst_x2[-1], n_seg))  # Recalcule les coordonnées x pour le deuxième segment
                        pente2 = (lst_y2[-1] - checkpoint[1]) / (lst_x2[-1] - checkpoint[0])  # Recalcule la pente pour le deuxième segment
                        c2 = checkpoint[1] - pente2 * checkpoint[0]  # Recalcule le coefficient de l'ordonnée à l'origine pour le deuxième segment
                        lst_y1.clear()  # Efface les coordonnées y pour le premier segment
                        lst_y2.clear()  # Efface les coordonnées y pour le deuxième segment
                        
                        for i in range(n_seg):
                            lst_y1.append(pente1 * lst_x1[i] + c1)  # Recalcule les coordonnées y pour le premier segment
                            lst_y2.ap

        self.x_dest.append((int)(checkpoint[0]))
        self.y_dest.append((int)(checkpoint[1]))

        
    def envoi_infos_mouvement(self):
        dist = []  # Liste pour stocker les distances parcourues
        orientation = []  # Liste pour stocker les orientations
        rotation = []  # Liste pour stocker les rotations

        u = np.array([0,-1],float)  # Vecteur u utilisé pour les calculs
        v = np.array([self.x_dest[-1]-self.x,self.y_dest[-1]-self.y],float)  # Vecteur v entre la position actuelle et la destination finale
        norm_v = math.sqrt((self.x_dest[-1]-self.x)**2 + (self.y_dest[-1]-self.y)**2)  # Norme du vecteur v
        int_product = np.dot(u,v)  # Produit scalaire entre les vecteurs u et v

        sign = np.sign((self.y_dest[-1]-self.y)/(self.x_dest[-1]-self.x))  # Signe pour l'orientation
        dist.append(norm_v)

        if sign > 0:
            if (self.x_dest[-1]-self.x) > 0:
                orientation.append(sign*math.acos(int_product/norm_v)*180/math.pi )
            else:
                orientation.append(sign*math.acos(int_product/norm_v)*180/math.pi + 180)
        else:
            if (self.x_dest[-1]-self.x) > 0:
                orientation.append(360+sign*math.acos(int_product/norm_v)*180/math.pi - 180)
            else:
                orientation.append(360+sign*math.acos(int_product/norm_v)*180/math.pi)
        
        rotation.append(orientation[-1]-self.angle)

        for i in range(len(self.x_dest)-1):
            v = np.array([self.x_dest[i+1]-self.x_dest[i],self.y_dest[i+1]-self.y_dest[i]],float)  # Vecteur v entre les destinations successives
            norm_v = math.sqrt((self.x_dest[i+1]-self.x_dest[i])**2 + (self.y_dest[i+1]-self.y_dest[i])**2)  # Norme du vecteur v
            int_product = np.dot(u,v)  # Produit scalaire entre les vecteurs u et v

            sign = np.sign((self.y_dest[i+1]-self.y_dest[i])/(self.x_dest[i+1]-self.x_dest[i]))  # Signe pour l'orientation
            dist.append(norm_v)

            if sign > 0:
                if (self.x_dest[i] - self.x_dest[i+1]) > 0:
                    orientation.append(sign*math.acos(int_product/norm_v)*180/math.pi)
                else:
                    orientation.append(sign*math.acos(int_product/norm_v)*180/math.pi + 180)
            else:
                if (self.x_dest[i] - self.x_dest[i+1]) > 0:
                    orientation.append(360+sign*math.acos(int_product/norm_v)*180/math.pi - 180)
                else:
                    orientation.append(360+sign*math.acos(int_product/norm_v)*180/math.pi)

            rotation.append(orientation[1]-orientation[0])

        f = open('infos_mouvement.txt','w')  # Ouverture du fichier pour écrire les informations de mouvement
        for j in range(len(dist)):
            f.write((str)(rotation[j])+' '+(str)(dist[j])+'\n')  # Écriture des rotations et des distances dans le fichier
        f.close()

        print("=================================================================================================")
        print("Longueur du premier segment :",dist[0],"cm "," Rotation necessaire :", rotation[0],"°")
        print("Longueur du premier segment :",dist[1],"cm "," Rotation necessaire :", rotation[1],"°")
        print("=================================================================================================")

        return rotation, dist  # Retourne les rotations et les distances

