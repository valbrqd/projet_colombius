#from tkinter import *
from Robot import *
import serial
import sys

"""
Fonction de calcul de la trajectoire du robot 
Input : coordonnees du click sur l'API Web
Output : transmission des informations sur la liaison serie
"""
def event_click(x_clic, y_clic):

    #adaptation des coord
    x_clic = (float)(x_clic)
    y_clic = (float)(y_clic)
    x_clic = round(x_clic/1.9)
    y_clic = round(y_clic/1.9)

    #liaison serie
    ser = serial.Serial('/dev/ttyUSB0', 19200)

    #dimension de la salle en cm
    long_map = 262
    largeur_map = 262

    #initialisation du robot
    x = -20
    y = -20
    x_robot = -20
    y_robot = -20
    vitesse = 1
    
    #creation du robot
    robot = Robot(x_robot, y_robot, vitesse, long_map, largeur_map)
    #met a jour la position du robot
    robot.set_position_init_robot()
    #met a jour la destination
    robot.set_destination(x_clic,y_clic)
    #calcul des infos utiles a la trajectoire
    rotation, dist = robot.start()
    
    #envoi des infos sur la liaison serie
    chain = ""
    for info in range(len(rotation)):
        if info == 0:
            chain = chain + '&'
        chain = chain + (str)(round(rotation[info],1))
        chain = chain + ","
        chain = chain + (str)(round(dist[info],1))
        if info != len(rotation)-1:
            chain = chain + ","
        if info == len(rotation)-1:
            chain = chain + "@"
    print("chain :",chain)
    ser.write(chain.encode())

#event_click(500, 500)
event_click(sys.argv[1], sys.argv[2])



