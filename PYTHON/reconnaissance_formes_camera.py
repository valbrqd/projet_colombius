import cv2
import numpy as np
import imutils
#import matplotlib.pyplot as plt
import time 
import os
#import serial

import Position.Positionnement as po

class ShapeDetector:
    def __init__(self):
        pass
    def detect(self, c):
        # Initialise la forme et le mur, calcule le perimetre de l'objet et ses sommets
        mur = 5 # pas de mur identifie
        shape = "non identifiee"
        peri = cv2.arcLength(c, True)
        aire = cv2.contourArea(c)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)
        
    
        #Condition sur la taille de l'objet pour choisir sa forme => evite d'identifier les bruits
        if 600 > peri > 80 :
            # if the shape is a triangle, it will have 3 vertices
            if len(approx) == 3:
                shape = "triangle"
                mur = 0
            
                
            # if the shape is a pentagon, it will have 5 vertices
            elif  5<= len(approx) <= 6:
                shape = "pentagone"
                mur = 1
                
            # elif len(approx) == 6:
            #     shape = "hexagone"
            #     mur = 2
            
            elif 9<= len(approx) <=12 :
                shape = "etoile"
                mur = 3
            
            elif len(approx) == 4:
                # compute the bounding box of the contour and use the
                # bounding box to compute the aspect ratio
                (x, y, w, h) = cv2.boundingRect(approx)
                ar = w / float(h)
                # a square will have an aspect ratio that is approximately
                # equal to one, otherwise, the shape is a rectangle
                if ar >= 0.95 and ar <= 1.05 :
                    shape = "square"
                    mur = 2
                    
                
                else : 
                    shape = "rectangle"
                    mur = 2
                # otherwise, we assume the shape is a circle

            else:
                shape = "cercle"
        #print("aire de ",shape, ": ",aire)
        # return the name of the shape
        return shape, mur, peri, aire

    def text_display(self, nom_image,lst_area,lst_shape, lst_coord,lst_wall) :
        image = cv2.imread(nom_image)
        max_area = max(lst_area)
        index_max = len(lst_area)+1
        for i in range(len(lst_area)) :
            textSize = cv2.getTextSize(lst_shape[i],cv2.FONT_HERSHEY_SIMPLEX,0.5,2)[0]
            if lst_area[i] == max_area and lst_shape[i] != "non identifiee" :
                index_max = i
                cX = lst_coord[index_max][0]
                cY = lst_coord[index_max][1]
                shapeF = lst_shape[index_max]
                #Affiche le texte de la plus grande forme en bleu (BGR)
                cv2.putText(image, shapeF, (int(cX-textSize[0]/2),int(cY+textSize[1]/2)), cv2.FONT_HERSHEY_SIMPLEX,1,(255,0,0), 2)
            
            if lst_shape[i] != "non identifiee" and i != index_max :
                cX = lst_coord[i][0]
                cY = lst_coord[i][1]
                cv2.putText(image, lst_shape[i], (int(cX-textSize[0]/2),int(cY+textSize[1]/2)), cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255), 2)
                print("Les autres formes reconnues sont : ", lst_shape[i])
        
        print('La forme principale est  : ', shapeF)
        wallF = lst_wall[index_max]
        return image, wallF
        
def get_pos() :

        # Initialisation du port sur lequel la commande est envoyee
    #ser = serial.Serial('/dev/ttyUSB0', 19200)
    # Enable camera
    cap = cv2.VideoCapture(0) #CAP_DSHOW is an API preference for directshow 

    cap.set(3, 640)
    cap.set(4, 420)

    if cap.isOpened()==False:
        print('Error opening video')

    # Initialiser le compteur de temps
    start_time = time.time()
    condition = True
    # plage de couleurs pour la couleur verte
    lower_color = np.array([118, 80, 41])
    upper_color = np.array([165, 130, 242])
    
    ####  Seuillage par couleur #######
        #test des differentes couleurs recuperees sur notre image pour faire les limites
    
    # rgb_color = np.array([39,26,41], dtype=np.uint8)

    # rgb_color1 = np.array([133,111,192], dtype=np.uint8)

    # rgb_color2 = np.array([46,27, 46], dtype=np.uint8)

    # hsv_color = cv2.cvtColor(np.array([[rgb_color]]), cv2.COLOR_RGB2HSV)[0][0] # conversion en valeurs HSV
    # hsv_color1 = cv2.cvtColor(np.array([[rgb_color1]]), cv2.COLOR_RGB2HSV)[0][0] # conversion en valeurs HSV
    # hsv_color2 = cv2.cvtColor(np.array([[rgb_color2]]), cv2.COLOR_RGB2HSV)[0][0] # conversion en valeurs HSV
    
    
    # print('couleurs HSV 1 : ', hsv_color)
    # print('couleurs HSV 2 : ', hsv_color1)
    # print('couleurs HSV 3 : ', hsv_color2)
        
    while condition  :
        eval, img = cap.read()
        imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        key = cv2.waitKey(1)
        
        if eval == False :
            print('erreur video')
            break

        current_time = time.time()
        elapsed_time = current_time - start_time
        #print("temps : ", elapsed_time)
        shape = "initial"
        lst_perim = []
        lst_shape = []
        lst_coord = []
        lst_area = []
        lst_wall = []

    

        if elapsed_time >= 6 :
            #Prise d'une photo de la webcam
            nom_image = 'imageI.png'
            cv2.imwrite(nom_image,img)
            
            # Ouverture de l'image
            image = cv2.imread(nom_image)
            
            # Afficher les valeurs HSV résultantes
            image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

            #Seuillage sur les valeurs de la plage donnee precedemment
            seuil = cv2.inRange(image_hsv, lower_color, upper_color)
        

            # Operations morphologiques pour remplir nos objets d'interets 
            # et eliminer les bruits
            iterations =6

            elem_cross = cv2.getStructuringElement(cv2.MORPH_CROSS,(5,3))
            elem_rect = cv2.getStructuringElement(cv2.MORPH_RECT,(5,2))
            dilate = cv2.dilate(seuil,elem_cross,iterations)
            erode = cv2.erode(dilate,elem_rect,iterations-2)

            #cv2.imwrite("image_seuillee.png", erode)

            # Recuperation des contours
            cnts = cv2.findContours(erode, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
            cnts = imutils.grab_contours(cnts) #Normalise les contours pour itérer plus facilement

            
            sd = ShapeDetector()
            compteur = 0
            # Boucle sur les contours
            for c in cnts:
                compteur+=1
                M = cv2.moments(c)
                if (M["m00"] ==0) :
                    M["m00"] =1
                
                # Recuperation du centre de la forme
                cX = int((M["m10"] / M["m00"]) )
                cY = int((M["m01"] / M["m00"]) )
                lst_coord.append([cX,cY])
                
                
                # Detection des formes
                shape, wall, perimeter, area = sd.detect(c)
            
                lst_perim.append(perimeter)
                lst_shape.append(shape)
                lst_area.append(area)
                lst_wall.append(wall)
                perim_max = max(lst_perim)

                # #Creation du masque pour recuperer les couleurs des formes
                mask = np.zeros(image.shape[:2], np.uint8)
                cv2.drawContours(mask, [c], -1,(255,255,255), -1)
                
                # #Convertion de l'image BGR (specifique à openCV) en RGB
                # imgRGB = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                
                
                if (shape == "triangle" or shape == "etoile" or shape == "hexagone" or shape == "pentagone" or shape == "carre" or shape == "rectangle") and area > 1000 :
                    cap.release()
                    
                    # cv2.destroyWindow('face_detect')
                    condition = False
                    

                else :
                    # Réinitialiser le compteur de temps
                    start_time = current_time
                    # Rotation du robot pour esperer trouver une forme
                    #ser.write("45".encode())

        

    image, wallF = sd.text_display(nom_image,lst_area,lst_shape,lst_coord,lst_wall)
    pos_x,pos_y = po.positionnement(wallF)
    
    #cv2.imwrite('Image_detectee.png', image)

    # Creation des contours des formes
    #cv2.drawContours(src, [c], -1, (R, G, B), 2) -> le "-1" signifie qu'on dessine tous
    #les contours et le 2 est le choix du remplissage 
    contours = cv2.drawContours(image,cnts, -1, (0,255,0), 2)
   
    os.remove('imageI.png')

    return pos_x, pos_y
    

if __name__ == '__main__' :
    get_pos()


    
    
       




           