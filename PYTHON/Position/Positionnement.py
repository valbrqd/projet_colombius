

try:
    from Lidar_infos import*
    from Calc_pos import *
    from Find_wall import *
    from Tri_info import *
    from Detec_bloc import *
    
except:
    from Position.Lidar_infos import*
    from Position.Calc_pos import *
    from Position.Find_wall import *
    from Position.Tri_info import *
    from Position.Detec_bloc import *

from matplotlib import pyplot as plt


"""
Fonction de positionnement du Robot Colombius
Input : mur se situant derriere le robot, info calculee par le programme de reconnaissance video
Output : position du robot dans la salle
"""
def positionnement(back):

    largeur_map = 263
    long_map = 263

    
    d,t = get_info()
   
    #tri des informations dans l'ordre des angles croissant
    dist,theta = get_info_tri(d,t)

    #placement des points en coordonnées cartésiennes
    [x,y] = get_pos(dist, theta)

    #affichage du nuage de point original
    plt.figure(1)
    plt.plot(y,x,"o")
    plt.title("Nuage de point avant traitement")
    plt.show()

    #on retire le bloc du nuage de point
    remove_bloc(x,y,theta)

    #affichage du nuage de point sans le bloc
    plt.figure(2)
    plt.plot(y,x,"o")
    plt.title("Nuage de point sans le bloc central")
    plt.show()

    lst_wallx,lst_wally,lst_wall,list_droite,ind_pt_back = get_wall(x,y,theta)#segmentation des murs

    nb_mur = len(lst_wall)#nombre de murs

    aff = ["ro","bo","yo","go","ko","mo","co","ro","bo","yo","go","ko","mo","co"]#couleurs des murs

    #Affichage de la segmentation du nuage de pointS
    plt.figure(3)
    for w in range(nb_mur):
        #detection du mur arriere
        if lst_wall[w][0] == 1:
            rotation = back - w
            plt.plot(lst_wally[w][1],lst_wallx[w][1],"k+")
        else:
            plt.plot(lst_wally[w][1],lst_wallx[w][1],aff[w])

    droite = []
    dist_droite = []
    list_min = []
    #affichage des regressions lineaires
    for wall in range(len(list_droite)):

        for p in range(len(lst_wallx[wall][1])):

            x = lst_wallx[wall][1][p]
            y = list_droite[wall][0]*x + list_droite[wall][1]

            droite.append(y)
            dist_droite.append(math.sqrt(x**2 + y**2))

        list_min.append(min(dist_droite))

        plt.plot(droite,lst_wallx[wall][1],"k-")
        droite = []
        dist_droite = []
    plt.title("Nuage de point segmenté par mur")
    plt.show()

    #Rotation des listes 
    lst_wallx = lst_wallx[-rotation:] + lst_wallx[:-rotation]
    lst_wally = lst_wally[-rotation:] + lst_wally[:-rotation]
    lst_wall = lst_wall[-rotation:] + lst_wall[:-rotation]
    list_droite = list_droite[-rotation:] + list_droite[:-rotation]
    list_min = list_min[-rotation:] + list_min[:-rotation]

    #calcul de l'angle de depart
    ind_back_dist_min = closest_point(dist, list_min[back])
    angle_depart = theta[ind_back_dist_min] - theta[ind_pt_back]

    #affichage des murs apres rotation
    plt.figure(4)
    for w in range(nb_mur):
        
        if lst_wall[w][0] == 1:
            plt.plot(lst_wally[w][1],lst_wallx[w][1],"k+")
        else:
            plt.plot(lst_wally[w][1],lst_wallx[w][1],aff[w])

    droite = []
    dist_droite = []
    list_min = []

    for wall in range(len(list_droite)):
        
        for p in range(len(lst_wallx[wall][1])):

            x = lst_wallx[wall][1][p]
            y = list_droite[wall][0]*x + list_droite[wall][1]

            droite.append(y)
            dist_droite.append(math.sqrt(x**2 + y**2))

        if lst_wallx[wall][1] != []:
            list_min.append(min(dist_droite))

        plt.plot(droite,lst_wallx[wall][1],"k-")
        plt.plot(0,0,"ro")
        droite = []
        dist_droite = []

    plt.title("Nuage de point segmenté par mur")
    plt.show()

    #Affichage des informations importantes
    print("===================================================================")
    print("Distance au mur ",back-1," : ", list_min[back-1])
    print("Distance au mur ",back," : ", list_min[back])

    print("L'angle entre la normale au mur ", back," et le robot est : ",angle_depart,"°")
    print("===================================================================")

    #Envoi des infos dans un fichier
    f = open('infos_positions.txt','w')
    f.write((str)(back-1) + ' ' +(str)(list_min[back-1]))
    f.write('\n' + (str)(back) + ' ' + (str)(list_min[back]))
    f.write('\n' + (str)(angle_depart))
    f.close()

    #Calcul des coordonnees du robot en cm
    alpha = back*90 + angle_depart - 180
    if alpha < 0:
        alpha = 360+alpha

    if back-1==-1 and back == 0 :
        x_robot = list_min[back-1]/10
        y_robot = list_min[back]/10
        
    if back-1==0 and back == 1 :
        x_robot = largeur_map-list_min[back-1]/10
        y_robot = list_min[back]/10

    if back-1==1 and back == 2 :
        x_robot = largeur_map-list_min[back-1]/10
        y_robot = long_map - list_min[back]/10

    if back-1==2 and back == 3 :
        x_robot = list_min[back-1]/10
        y_robot = long_map - list_min[back]/10

    return x_robot*1.9,y_robot*1.9


back = 0
positionnement(back)
