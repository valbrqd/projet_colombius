from rplidar import RPLidar

def get_info():

    try:
        import serial
        port = serial.Serial('/dev/ttyUSB0')
        port_name = '/dev/ttyUSB0'
        lidar = RPLidar(port_name)
        
    except:
        lidar = RPLidar('com7')

    info = lidar.get_info()
    print(info)

    health = lidar.get_health()
    print(health)

    lst_info = []

    n = 0;
    for i in lidar.iter_scans():
        if n < 1:
            lst_info.append(i)
            n+=1
        else:
            break

    lidar.stop()
    lidar.stop_motor()
    lidar.disconnect()

    lst_dist = []
    lst_angles = []

    for seq in lst_info:
        for info in seq:
            lst_angles.append(info[1])
            lst_dist.append(info[2])
    return lst_dist,lst_angles