from matplotlib import pyplot as plt
import math
from scipy.stats import linregress

#Fonction: segmentation des murs
#Entree: nuages de point (x,y)
#Sortie: lst_wallx,lst_wally : pour l'affichage
#        lst_wall : pour savoir quel mur est celui derrière le Lidar
#        final_lst_droite : pour tracer les courbes de tendances et calculer les distances minimales
#        ind_pt_back : pour connaitre l'indice du point a 180° du robot

def get_wall(x,y,theta):
    #initialisation de la liste des murs
    lst_wallx = []
    lst_wally = []  
    lst_wall_dist = []

    #initialisation des courbes de tendances
    lst_droite = []

    n = len(x)#nombres de points a traiter 

    for i in range(n-2):
        #on choisi un point
        x1 = x[i]
        y1 = y[i]
        #on calcul la pente avec le point suivant
        x2 = x[i+1]
        y2 = y[i+1]

        if x2-x1 == 0 and y2-y1 ==0:
            x2+=0.001

        #calcul de la pente normalisée en x et en y
        penteX = (x2-x1)/math.sqrt((x2-x1)**2+(y2-y1)**2)
        penteY = (y2-y1)/math.sqrt((x2-x1)**2+(y2-y1)**2) 

        sens = 0.9 #sensibilite de la detection de mur
        allone = True

        for n in range(len(lst_wallx)):

            #si il n'y a pas de trop grand changement de pente, on est sur le même mur
            if abs(penteX - lst_wallx[n][0]) < sens and abs(penteY - lst_wally[n][0]) < sens:
                
                lst_wallx[n][1].append(x1)
                lst_wally[n][1].append(y1)
                lst_wall_dist[n][1].append(math.sqrt(x1**2 + y1**2))
                
                #detection du mur derriere le Lidar en prenant le point a l'angle 180° 
                if theta[i]<=181 and theta[i]>=179:
                    lst_wall_dist[n][0] = 1
                    ind_pt_back = i
                allone = False

                """
                x_new_pente = lst_wallx[n][1][-1]
                y_new_pente = lst_wallx[n][1][-1] 

                lst_wallx[n][0] = (lst_wallx[n][1][-1] - lst_wallx[n][0][0])
                lst_wally[n][0] = (lst_wally[n][1][-1] - lst_wally[n][0][0])
                """

                break
        
        #creation d'un nouveau mur si la pente detectee est nouvelle
        if allone:
            lst_wallx.append([penteX, [x1]])
            lst_wally.append([penteY, [y1]])
            lst_wall_dist.append([0, [math.sqrt(x1**2 + y1**2)]])

    #creation des premieres courbes de tendances
    for w in range(len(lst_wallx)):
        slope, intercept, r_value, p_value, std_err = linregress(lst_wallx[w][1], lst_wally[w][1])
        lst_droite.append([slope,intercept]) 

    #reajustement des murs en fonction des courbes de tendances
    lst_wallx,lst_wally,lst_wall_dist = fusion_wall(lst_wallx, lst_wally, lst_wall_dist, lst_droite)

    final_lst_droite = []

    #reajustement des courbes de tendances
    for new_w in range(len(lst_wallx)):
        if len(lst_wallx[new_w][1])>15:
            new_slope, new_intercept, new_r_value, new_p_value, new_std_err = linregress(lst_wallx[new_w][1], lst_wally[new_w][1])
            final_lst_droite.append([new_slope,new_intercept]) 

    return lst_wallx,lst_wally,lst_wall_dist,final_lst_droite, ind_pt_back

#Fonction: affinage des murs
#Entree: nuages de points des murs en x et y, représentation des murs par la distance, liste des coefficents des regressions lineaires des murs
#Sortie: Nouveaux murs en x et y et en dist
def fusion_wall(lst_wallx, lst_wally, lst_wall_dist, lst_droite):

    for wall_ind in range(len(lst_wall_dist)):

        lst_exchange = []
        
        #on determine quels points doivent changer de mur
        for point_ind in range(len(lst_wall_dist[wall_ind][1])):
            lst_indice = []
            
            for i in range(len(lst_wallx)):
                if len(lst_wallx[i][1])!=0:
                    lst_indice.append(closest_wall(lst_wallx[i], lst_wallx[wall_ind], lst_wally[wall_ind], point_ind, lst_droite[i])) 
            
            ind_mini = 0
            mini = lst_indice[0]
            
            for i in range(len(lst_indice)):
                if lst_indice[i] < mini:
                    mini = lst_indice[i]
                    ind_mini = i

            lst_exchange.append([ind_mini,point_ind])

        #modification des murs
        x = []
        y = []
        d = []
        for e in lst_exchange:
            if e[0] != wall_ind:
                x.append([e[0],lst_wallx[wall_ind][1][e[1]]])
                y.append([e[0],lst_wally[wall_ind][1][e[1]]])
                d.append([e[0],lst_wall_dist[wall_ind][1][e[1]]])

        for j in range(len(x)):
            lst_wallx[wall_ind][1].remove(x[j][1])
            lst_wally[wall_ind][1].remove(y[j][1])
            lst_wall_dist[wall_ind][1].remove(d[j][1])

            lst_wallx[x[j][0]][1].append(x[j][1])
            lst_wally[y[j][0]][1].append(y[j][1])
            lst_wall_dist[d[j][0]][1].append(d[j][1])

    return lst_wallx, lst_wally, lst_wall_dist

#Fonction: reconnais la distance minimale d'un point avec un mur
#Entree: lst des coordonnees en x des murs, mur etudie en x et y, indice du point etudie, coeficients de la regression lineaire du mur 
#Sortie: distance min entre le point et le mur etudies
def closest_wall(lst_wallx, current_lst_wallx, current_lst_wally, point_ind, droite):

    dist = []
    for p in range(len(lst_wallx[1])):
        y = droite[0]*lst_wallx[1][p] + droite[1] - current_lst_wally[1][point_ind]
        x = lst_wallx[1][p]-current_lst_wallx[1][point_ind]
        dist.append(math.sqrt(x**2 + y**2))
    
    dist_min = min(dist)

    return dist_min


#Fonction: calcul de l'indice du point dont la distance est la plus proche de la distance minimale du robot a la regression lineaire
#Entree: lst_dist: liste des distances triees dans l'ordre des angles croissants
#        dist: distance minimale entre le robot et le mur derriere lui
#Sortie: indice du point dont la distance est la plus proche de la distance minimale du robot a la regression lineaire
def closest_point(lst_dist, dist):
    return lst_dist.index(lst_dist[min(range(len(lst_dist)), key = lambda i: abs(lst_dist[i]-dist))])


