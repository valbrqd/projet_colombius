import math

def get_pos(dist,theta):

    n = len(dist)

    x = []
    y = []

    for i in range(n):
        x.append(-math.sin(theta[i]*math.pi/180)*dist[i])
        y.append(math.cos(theta[i]*math.pi/180)*dist[i])

    return x,y