import numpy as np

def get_info_tri(dist,theta):

    n = len(theta)
    for i in range(n-1,0,-1):
        for j in range(i):
            if theta[j]>theta[j+1] : 

                dist[j+1],dist[j] = dist[j],dist[j+1]
                theta[j+1],theta[j] = theta[j],theta[j+1]
                
    return dist,theta
