import math

def remove_bloc(pos_x, pos_y, theta):

    lst_pts = [[],[]]
    for p in range(len(pos_x)):
        lst_pts[0].append(pos_x[p])
        lst_pts[1].append(pos_y[p]) 

    bloc_flag = False

    for index in range(len(pos_x)-1):
            
            dist = math.sqrt(lst_pts[0][index]**2 + lst_pts[1][index]**2)
            next_dist = math.sqrt(lst_pts[0][index+1]**2 + lst_pts[1][index+1]**2)
            
            if bloc_flag == True and dist < next_dist - 200 :
                bloc_flag = False
                break
            if dist > next_dist + 200 and theta[index]-theta[index+1]<20:
                bloc_flag = True

            if bloc_flag == True:
                pos_x.remove(lst_pts[0][index+1])
                pos_y.remove(lst_pts[1][index+1])               
            